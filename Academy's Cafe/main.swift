import Foundation

// TODO: You may add/edit/remove any default code here

func showMainMenu() {
    print("""

    =================================
       Academy's Cafe & Resto v2.0
    =================================

    Options:
    [1] Buy Food
    [2] Shopping Cart
    [x] Exit

    """)
    
    print("Your choice? ", terminator: "")
}

showMainMenu()

readLine()
